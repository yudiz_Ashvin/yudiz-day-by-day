var a ;
let b ;
console.log(a); //undefined
console.log(b);//undefined

//==============ReferenceError:-

console.log(a); //undefined
console.log(b); //ReferenceError: Cannot access 'b' before initialization

var a = 50;
let b = 10;


//----------------
//================SyntaxError

var a ;
const b ; //SyntaxError: Missing initializer in const declaration
//hall code is block
console.log(a); 
console.log(b);

//-----------------

var a =10;
var a = 20;
let b = 30;
let b  = 40; //SyntaxError: Identifier 'b' has already been declared
console.log(a); 
console.log(b);


//but you can do like this in let

var a =10;
var a = 20;
let b = 30;
b  = 40; 
console.log(a); //20
console.log(b); //40


//------------------------------
//===================TypeError: Assignment to constant variable. in const

var a =10;
var a = 20;
const b = 30;
b  = 40; 
console.log(a); //20
console.log(b); //40


//------------------------------
//ReferenceError: Cannot access 'b' before initialization
//let and const behave as same in hosting

console.log(b); 
const b = 30;
b  = 40; 
