// 51. Write a JavaScript program to convert a given number to hours and minutes.
 function convert(num){
     var hours = Math.floor(num/60);
     var minutes  = num % 60;

     return hours + ":" + minutes;
 }
// console.log(convert(71)); 
// console.log(convert(450)); 
// console.log(convert(1441));


// 52. Write a JavaScript program to convert the letters of a given string in alphabetical order.

function convert(str){
    return str.split('').sort().join('');
    // return str.split('').reverse().join('');

}

// console.log(convert('Ashvin'))
// console.log(convert('zyxwvu'))
// console.log(convert('zyxwvutsrqponmlkjihgfedcba'))

// 53. Write a JavaScript program to check whether the characters a and b are separated by exactly 3 places anywhere (at least once) in a given string.

// Definition and Usage
// The test() method tests for a match in a string.

// If it finds a match, it returns true, otherwise it returns false.

function check(str){
    return (/a...b/).test(str) ||(/b...a/).test(str);
}

// console.log(check("Chainsbreak"));
// console.log(check("pane borrowed"));
// console.log(check("abCheck"));


// 54. Write a JavaScript program to count the number of vowels in a given string.

function check(str) { 

    // find the count of vowels
    return str.match(/[aeiou]/gi).length;// .length for count
    // return number of vowels
}
// console.log(check('Ashvin'))
// console.log(check("Python"));
// console.log(check("w3resource.com"));

// // take input
// const string = prompt('Enter a string: ');
// const result = countVowel(string);
// console.log(result);

// 55. Write a JavaScript program to check whether a given string contains equal number of p's and t's.
function check(str){
    let p = str.match(/[^p]/g).length;
    let t = str.match(/[^t]/g).length;
    return p===t;
}
// console.log(check("patatpss asthtandpp"));
// console.log(check("paatps"));
// 56. Write a JavaScript program to divide two positive numbers and return a string with properly formatted commas.

function check(n1, n2){
    var divi = Math.floor(n1/n2);
    return divi.toString().split('').sort();
    // return divi;
}
// console.log(check(125,3))

// 57. Write a JavaScript program to create a new string of specified copies (positive number) of a given string.
function copy(str, num){
    if(num <= 0){
        return false;
    }
    return str.repeat(num);
}
// console.log(copy("abc", 5));
// console.log(copy("abc", 0));
// console.log(copy("abc", -2));

// 58. Write a JavaScript program to create a new string of 4 copies of the last 3 characters of a given original string. The length of the given string must be 3 and above.

function copy(str)
{
  if (str.length >= 3) {
    return str.substring(str.length - 3).repeat(4);
  }
  else
    return false;
}
// console.log(copy("Python 3.0"));
// console.log(copy("JS"));
// console.log(copy("JavaScript"));


// 59. Write a JavaScript program to extract the first half of a string of even length.
function print(str){
    if(str.length%2 == 0){ // for chech str is even
        return str.slice(0, str.length / 2);
    }
    return str;
}
// console.log(print('Ashvin'))
// console.log(print('Ashvinvanol'))

// 60. Write a JavaScript program to create a new string without the first and last character of a given string.

function print(str){
    return str.slice(1 , str.length-1);
    // return str.substring(1, str.length - 1);
}
// console.log(print('Ashvinvanol'))
// console.log(print('JavaScript'));
// console.log(print('JS'));
// console.log(print('PHP'));

