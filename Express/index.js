const express = require('express');
const cors = require('cors')
const helmet = require('helmet')
const app = express();
require('dotenv').config();

const bodyParser = require('body-parser');
const req = require('express/lib/request');
app.use(bodyParser.json()) //middleware
app.use(cors())
app.use(helmet())

// app.get('/' , (req,res) => {
//     // res.send({ obj: 'ping'})
//     // res.json('hello world') 
//     res.send('hello world')
// })

//using query
app.get('/', (req,res) => {
    res.json({ res : req.query.id})
})
app.get('/', (req,res) => {
    res.send("hello this is home page")
})
//using params

app.get('/users/:userId/:name/:email?', (req,res) => {
    res.json(req.params)
})
// http://localhost:3000/user?pli=1&authuser=0
app.get('/user', (req,res) => {
    res.json({ res : req.query.pli, 
        auther: req.query.authuser})
})

// body
app.post('/username', (req,res,next)=>{
    console.log(req.body);
    next()
},(req,res) => {
    // res.json({ user: req.body})
    res.send('ping')
})
// headers
app.post('/headers', (req,res) => {
    // res.json({ headers: req.headers })// all headers
    res.json({ headers: req.header('auth')}) // single headers
})
app.all('*', (req,res) => {
    res.json("no rout found")
})

const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
    console.log(`server run in port ${PORT}`);
});