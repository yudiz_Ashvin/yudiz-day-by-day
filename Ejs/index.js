const http = require('http');
const fs = require('fs');
const ejs = require('ejs')
const PORT = 3000
const arr = ['This is ejs example', 10]


const server = http.createServer((req, res) => {
    if (req.url === '/') {
        const htmlContent = fs.readFileSync(__dirname + '/views/main.ejs', 'utf8');
        const htmlRenderized = ejs.render(htmlContent, { name:'Ashvin'});
        res.writeHead(200, { 'Content-Type': 'text/html' });
        res.write(htmlRenderized, 'utf-8');
        res.end();
    }
    if (req.url === '/user') {
        const data = { name : 'ashvin',
                     email: 'ashvin@gmail.com',
                     pass: '123321'
    }
        const htmlContent = fs.readFileSync(__dirname + '/views/index.ejs', 'utf8');
        const htmlRenderized = ejs.render(htmlContent, { filename: 'index.ejs', data });
        res.writeHead(200, { 'Content-Type': 'text/html' });
        res.write(htmlRenderized, 'utf-8');
        res.end();
    }
    if (req.url === '/about') {
        const filePath = __dirname + '/public' + `${req.url}.html`;
        fs.readFile(filePath, (err, data) => {
            res.writeHead(200, { 'Content-Type': 'text/html' });
            res.write(data, 'utf-8');
            res.end();
        })
    }
});

server.listen(PORT, () => {
    console.log(`Listening on Port: ${PORT}`);
});



