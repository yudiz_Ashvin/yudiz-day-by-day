// 71. Write a JavaScript program to check whether 1 appears in first or last position of a given array of integers. The array length must be greater or equal to 1.
function chech(num){
    let ans = num.toString().split('');
    return ans[0] == 1 || ans[num.length-1]==1;
}
// console.log(chech(135));
// console.log(chech(1351));
// console.log(chech(246));

function chech(nums)
{
  var end_pos = nums.length - 1;
  return nums[0] == 1 || nums[end_pos] == 1;
}
// console.log(chech([1, 3, 5]));
// console.log(chech([1, 3, 5, 1]));
// console.log(chech([2, 4, 6]));


// 72. Write a JavaScript program to check whether the first and last elements are equal of a given array of integers length 3.

function check(num){
    if(num.length >= 1 ){
        return num[0] == num[num.length-1]
    }
    return false;
}
// console.log(check([10, 20, 30])); 
// console.log(check([10, 20, 30, 10])); 
// console.log(check([20, 20, 20]));

// 73. Write a JavaScript program to reverse the elements of a given array of integers length 3.

function reverse3(arr){
    return arr.reverse();
}
// w3 solution

function reverse3(array) {
    return array.map((element, idx, arr) => arr[(arr.length - 1) - idx]);
}
// console.log(reverse3([5, 4, 3])); 
// console.log(reverse3([1, 0, -1]));  
// console.log(reverse3([2, 3, 1]));

// 74. Write a JavaScript program to find the larger value between the first or last and set all the other elements with that value. Display the new array.

function all_max(nums) 
 {
    var max_val = nums[0] > nums[2] ? nums[0] : nums[2];
    nums[0] = max_val;
    nums[1] = max_val;
    nums[2] = max_val;

    return nums;
}
// console.log(all_max([20, 30, 40]));
// console.log(all_max([-7, -9, 0]));
// console.log(all_max([12, 10, 3]));

// 75. Write a JavaScript program to create a new array taking the middle elements of the two arrays of integer and each length 3.

function mid(num){
    var ans = [];
     ans.push(x[1],y[1]);
     return ans;
}
// console.log(mid([1, 2, 3], [1, 5, 6]));  
// console.log(mid([3, 3, 3], [2, 8, 0]));  
// console.log(mid([4, 2, 7], [2, 4, 5])); 


// 76. Write a JavaScript program to create a new array taking the first and last elements from a given array of integers and length must be greater or equal to 1.

function mid(num){
    var ans = [];
     ans.push(num[0], num[num.length - 1]);
     return ans;
}
// console.log(mid([20, 20, 30]));
// console.log(mid([5, 2, 7, 8]));
// console.log(mid([17, 12, 34, 78])); 


// 77. Write a JavaScript program to test whether an array of integers of length 2 contains 1 or a 3.

function chech(num){
    return (/1/).test(num) || (/3/).test(num)
}

// w3 solution

function contains13(nums) {

    if (nums.indexOf(1) != -1 || nums.indexOf(3) != -1){
       return true
    } 
    else
    {
       return false
    }
}
// console.log(chech([1, 5]));  
// console.log(chech([2, 3]));  
// console.log(chech([7, 5])); 

// 78. Write a JavaScript program to test whether an array of integers of length 2 does not contain 1 or a 3.
function chech(num){
    return num.includes(1)|| num.includes(3)?false:true;
}

// w3 solution

function chech(nums) {

    if (nums.indexOf(1) == -1 && nums.indexOf(3) == -1){
       return true
    } 
    else
    {
       return false
    }
}
// console.log(chech([7, 8]));
// console.log(chech([3, 2]));
// console.log(chech([0, 1])); 


// 79. Write a JavaScript program to test whether a given array of integers contains 30 and 40 twice. The array length should be 0, 1, or 2.
function twice3040(arra1) {
    let a = arra1[0],
        b = arra1[1];
    return (a === 30 && b === 30) || (a === 40 && b === 40);
}

console.log(twice3040([30, 30]));
console.log(twice3040([40, 40]));
console.log(twice3040([20, 20]));
console.log(twice3040([30]));




// 80. Write a JavaScript program to swap the first and last elements of a given array of integers. The array length should be at least 1.


function swap(arr){
        var temp =arr[0];
        arr[0] = arr[arr.length-1];
        arr[arr.length-1] = temp;
        
        return arr;
}
// console.log(swap([1, 2, 3, 4]));  
// console.log(swap([0, 2, 1]));  
// console.log(swap([3])); 
for(let i=0; i<arr.length-1; i++){
    if(arr[i] > 4) return true;
}