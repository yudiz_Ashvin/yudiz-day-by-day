1) Can scopes be reassigned at execution phase? 

Ans:-No
//===========================================================

console.log(a);
let a ="world";
Ans:-ReferenceError: Cannot access 'a' before initialization

//===========================================================

let a = "hello";
{
    let a ="World";
}
console.log(a);
Ans:-hello

//===========================================================

foo();
var foo = () =>{
    console.log("hello");
}
Ans:-TypeError: foo is not a function

//===========================================================

var a = "hello";
function myFunc(){
    console.log(a);
    // var a = "world";
}
myFunc();
Ans:-undefined

//===========================================================

In which scope is bar in? *

function bar(){
    var a = "hello";
}
Ans:-global scope

//===========================================================

function myFunc(){
    function hello(){
        console.log("hello world");
    }
}
myFunc();
hello();
Ans:-ReferenceError: hello is not defined

//===========================================================

In which scope is "bar" in? 
var foo = function bar() {
    var a = "hello";
}
Ans:-in it's own scope

//===========================================================

Which position is variable "a" down below in? 
console.log(a);
Ans:-source

//===========================================================

Can one and the same identifier exist in two different scopes? *
Ans:-No
//===========================================================

function foo(a){
    return function bar(){
        console.log(a);
    }
}
var baz = foo("hello world");
baz();

Ans:-hello world

//===========================================================

foo();

function foo(){
    console.log("hello");
}
Ans:-hello

//===========================================================

foo();

const foo = ()=>{
    console.log("hello");
}
Ans:-ReferenceError: Cannot access 'foo' before initialization

//===========================================================

Following is not a unit of scope. (Does not create it's own scope) *
Ans:-Object

//===========================================================

let a ='hello';
{
    a = 'world';
}
console.log(a);
Ans:-world

//===========================================================

What reference is the parameter "a" in on LINE: 1 *

function myFunc(a){
    return a;
}

Ans:-target

//===========================================================

What position is the variable "b" on LINE 2 *

function myFunc(a){
    anotherFunc(b);
}
Ans:-source

//===========================================================

const a = 'hello';
{
    const a = 'world';
}
console.log(a);
Ans:-hello

//===========================================================

console.log(a);
var a = "hello";
Ans:-undefined

//===========================================================

(function foo(a){
    return a
}) ()
foo("hello")
Ans:-ReferenceError: foo is not defined

//===========================================================

What is the role of Scope Manager? *
-->Creating buckets of scope when encountering function or a block.
-->Do mapping of all identifiers in the lexical environment they are declared or accessed in.
-->Handing out the identifiers from their respective scopes to the execution engine
Ans:-All of the above

//===========================================================

Which position is the variable "a" down below in? *
var a = 100;
Ans:-target

//===========================================================

for(let i=0; i<3; i++){
    setTimeout(function timeoutCd(){
        console.log((i));
    }, 0)
}
Ans:-012

//===========================================================

const foo ={
    a: "first",
    b: "second"
}
foo.a = 1;
console.log(foo);
Ans:-{ a: 1, b: 'second' }

//===========================================================

var foo = function bar(){
    console.log("hi");
}
// foo();// hi
bar();
Ans:-ReferenceError: bar is not defined

//===========================================================

var a = "hello"
function foo() {
    console.log(a);
}
a = "world";
foo();

Ans:-world

//===========================================================
 
What happens when you try to execute an identifier which is a variable storing primitive value. 
Ans:-Type Error

//===========================================================
Lexical Scoping is done..... *
Ans:-While your code is compiled & parsed

//===========================================================

var foo = function bar(){
    function baz(){

    }
}
Ans:-in scope of bar

//===========================================================

var a ="hello";
function myFunc(){
    var a = "world";
}
console.log(a);
Ans:-hello

////===========================================================

let a ="hello";
function myFunc(){
    console.log(a);
    let a = "world";
}
myFunc();
TDZ
Ans:-ReferenceError: Cannot access 'a' before initialization

//===========================================================

for(var i=0; i<3; i++){
    setTimeout(function timeoutCd(){
        console.log((i));
    }, 0)
}
Ans:-333

//===========================================================

How many steps does the Javascript virtual machine go through in order to execute the code

Ans:-2
//===========================================================

console.log(a);
var a = 'world';
Ans:-undefind

//===========================================================
What is bar? *

var foo = function bar(){
}
Ans:-function expression

//===========================================================

var a = 'hello';
foo();

function foo(){
    console.log(a);
    var a = "world"
}
Ans:-undefined

//===========================================================

function myFunc(){
    a =100;
    // console.log(a); //100

}
console.log(a);
myFunc()
Ans:-ReferenceError: a is not defined

//===========================================================

In which scope does "b" exist *

function foo(a){
    if(a>10){
        var b =100;
    }
}
foo(20);
Ans:-Scope of the function foo

//===========================================================

Javascript is a ____ language. *
Ans:-Compiled

//===========================================================

in which scope does foo exist? *
(function foo() {

})()
Ans:-in its own scope

//===========================================================

var a = "hello";
function myFunc(){
    console.log(a);
}
myFunc();
Ans:-hello

//===========================================================

foo is? *
(function foo(){

})
Ans:-function expression

//===========================================================

In which scope is the variable "a" in? *
var foo = function bar(){
    var a = "hello"
}
Ans:-in the scope of bar

//===========================================================

function myFunc(){
    a =100;
}
myFunc()
console.log(a);
Ans:-100

//===========================================================
During compilation phase, the compiler will remember____ *

Ans:-The scopes in which all the identifiers go into
 
Ans:-The positions in which all the identifiers are being uses in different places (target/source)

//===========================================================

var a = "hello";
{
    var a = "world";
}
console.log(a);
Ans:-world

//===========================================================

var a = "hello";
// a  = "world" // world

function foo(){
    console.log(a);
}

foo();
a  = "world"

Ans:-hello
