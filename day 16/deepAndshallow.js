// // Deep Vs Shallow Copy
// const a={id:1,name:"Vikas"} //original object
// // cloning object a to b
// const b=a  //cloned object
// //change the id value of b object
// b.id=5
// console.log("A value ",a)
// console.log("B value ",b)

// // // Deep copy
// let var1=4;
// let var2=var1;
// var2=10;
// console.log("var1 value ",var1);
// console.log("var2 value ",var2);

// // const a={id:1,name:"Vikas",address:{flatNo:23}} //original object
// // // cloning object a to b

// // //using spread operator
// // // const b={...a}  //cloned object

// // // using assign operator
// // // const b=Object.assign({},a)

// // //using stringify and parse
// // const b=JSON.parse(JSON.stringify(a))

// // //change the id value of b object
// // b.address.flatNo=5
// // b.id=5
// // console.log("A value ",a)
// // console.log("B value ",b)

// // Array 
// // const arr1=[1,2,3]
// // const arr2=[...arr1];
// // arr2[0]=5
// // console.log("Arr1 ",arr1)
// // console.log("Arr2 ",arr2)



// const a = 5
// let b = a // this is the copy
// b = 6
// console.log(b) // 6
// console.log(a) // 5

// shallow copy

// const a = {
//     en: 'Hello',
//     de: 'Hallo',
//     es: 'Hola',
//     pt: 'Olà'
//   }
//   let b = a
//   b.pt = 'Oi'
//   console.log(b.pt) // Oi
//   console.log(a.pt) // Oi

//Spread operator

//   const a = {
//     en: 'Bye',
//     de: 'Tschüss'
//   }
//   let b = {...a}
//   b.de = 'Ciao'
//   console.log(b.de) // Ciao
//   console.log(a.de) // Tschüss


// shallow copy

// let a = {
//     name: "ashvin",
//     age : 21
// }
// let b = a;

// //two way to solve it
// //1
// // let b = { ...a } 
// //2
// // b = Object.assign({}, a);

// b.name = "Ashvin"
// console.log(a);
// console.log(b);
// take reference not value

// import _ = require('lodash');

let dev = {
    name : "Ashvin",
    skills : {
        primary : 'full stack',
        secondary: 'devOps',
    },
    age: function(age){
        return 21;
    },        
    jodate : new Date()


}
// let dev2 = dev;
// dev2 = Object.assign({}, dev);
// let dev2 = {...dev}
// dev2.name = "Ajay";
let dev2 = JSON.parse(JSON.stringify(dev));
// let dev2 = _.cloneDeep(dev)
dev2.skills.primary = 'front end';

console.log(dev);
console.log(typeof(dev.jodate));
console.log(dev2);
console.log(typeof(dev2.jodate));



