// difference between slice and substring

// 61. Write a JavaScript program to concatenate two strings except their first character.
function concatenate(str1, str2) {
    return str1.substring(1, str1.length) + str2.substring(1, str2.length);
}
// console.log(concatenate("PHP","JS"));
// console.log(concatenate("A","B"));
// console.log(concatenate("AA","BB"));

// 62. Write a JavaScript program to move last three character to the start of a given string. The string length must be greater or equal to three.
function move(str) {
    if (str.length >= 3) {
        let fitst = str.substring(0, str.length - 3);
        let second = str.substring(str.length - 3, str.length);
        return second + fitst;
    }
    return str;
}

// // second method

function move(str) {
    if (str.length > 1) {
        return str.slice(-3) + str.slice(0, -3);
    }
    return str;
}

// console.log(move("Ashvin"))
// console.log(move("in"))
// console.log(move("Python"));
// console.log(move("JavaScript"));
// console.log(move("Hi"));

// 63. Write a JavaScript program to create a string using the middle three characters of a given string of odd length. The string length must be greater or equal to three.
function move(str) {
    if (str.length >= 3) {
        if (str.length % 2 != 0) {
            mid = (str.length + 1) / 2;
            return str.slice(mid - 2, mid + 1);
        }
        return str;
    }
}
// console.log(move('abcdefg'));
// console.log(move('HTML5'));
// console.log(move('Python'));
// console.log(move('PHP'));
// console.log(move('Exercises'));
// console.log(move('Ashvin'));

// 64. Write a JavaScript program to concatenate two strings and return the result. If the length of the strings are not same then remove the characters from the longer string.
function concatenate(str1, str2) {
    if (str1.length != str2.length) {
        const m = Math.min(str1.length, str2.length);

        return str1.substring(str1.length - m) + str2.substring(str2.length - m);
    }
    return str1 + str2;
}
// second solution

function concatenate(str1, str2) {
    const m = Math.min(str1.length, str2.length);

    return str1.substring(str1.length - m) + str2.substring(str2.length - m);
}
// console.log(concatenate("Python", "JS"));
// console.log(concatenate("ab", "cdef"));
// console.log(concatenate("abcd", "cdef"));

// 65. Write a JavaScript program to test whether a string end with "Script". The string length must be greater or equal to 6.
function check(str) {
    if (str.length >= 6) {
        return str.slice(str.length - 6, str.length) == "Script";
    }
}
// console.log(check("JavaScript"));
// console.log(check("Java Script"));
// console.log(check("Java Scripts"));

// 66. Write a JavaScript program to display the city name if the string begins with "Los" or "New" otherwise return blank.

function check(str) {
    if (str.slice(0, 3) == "Los" || str.slice(0, 3) == "New") {
        return str;
    }
    return " ";
}
// console.log(check("New York"));
// console.log(check("Los Angeles"));
// console.log(check("London"));

// 67. Write a JavaScript program to create a new string from a given string, removing the first and last characters of the string if the first or last character are 'P'. Return the original string if the condition is not satisfied.
function check(str){
    if(str.charAt(0)== 'P' || str.charAt(str.length-1) == 'P'){
        return str.slice(1,str.length-1);
    }
    return str;
}
// console.log(check('PeopleP'));
// console.log(check('ashvin'));
// console.log(check("PythonP"));
// console.log(check("Python"));
// console.log(check("JavaScript"));

// 68. Write a JavaScript program to create a new string using the first and last n characters from a given sting. The string length must be greater or equal to n.
function check(str,num){
    if(str.length >num){
    return str.substring(0, num) + str.substring(str.length - num);
    }
 return false;
}
// console.log(check("JavaScript", 2));
// console.log(check("JavaScript", 3));
// console.log(check("Jaafg", 3));

//-------------------------------------------------------------Array problems----------------------------------------------------------------------------


// 69. Write a JavaScript program to compute the sum of three elements of a given array of integers of length 3.
function sum(num1,num2,num3){
    return num1 + num2 + num3;
}
// console.log(sum(15,20,25))
// console.log(sum(10, 32, 20));  
// console.log(sum(5, 7, 9)); 
// console.log(sum(0, 8, -11));


// 70. Write a JavaScript program to rotate the elements left of a given array of integers of length 3.
function rotate(arr){
    // return arr.reverse(0);
    return [arr[1], arr[2], arr[0]];


}
console.log(rotate([3, 4, 5]));  
console.log(rotate([0, -1, 2]));  
console.log(rotate([7, 6, 5])); 
