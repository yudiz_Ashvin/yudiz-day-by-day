/*
PROBLEM - 1
   An isogram is a word that has no duplicate letters.
   Create a function that takes a string and returns either true or false depending on whether or not it's an "isogram".

   NOTES : Ignore letter case (should not be case sensitive).
*/
//==========  using for loop


function isIsogram(word) {
    let str = word.toUpperCase();
    let ans = str.split('').sort();
    for (var i = 0; i <= ans.length; i++) {
        if (ans[i - 1] == ans[i]) {
            return false;
        }
    }
    return true;
}

// second using set

function isIsogram(word) {
    let iso = new Set([...word]);
    return iso.size === word.length;
    //     // return iso;
    //     // Set(8) { 'A', 'L', 'G', 'O', 'R', 'I', 'S', 'M' }
    //     // Set(7) { 'P', 'A', 'S', 'W', 'O', 'R', 'D' }
    //     // Set(9) { 'C', 'O', 'N', 'S', 'E', 'U', 'T', 'I', 'V' }
}

// third solution

function isIsogram(word) {
    const obj = {};
    for(let char of word){
        const ascii = char.charCodeAt(0);
        if(ascii > 90) char = String.fromCharCode(ascii - 32);
        obj[char] > 0 ? (obj[char] += 1) : (obj[char] = 1);
        if(obj[char] > 1) return false;
    }
    return true;
}

console.log(isIsogram("Algorism"));      // true
console.log(isIsogram("PasSword"));      // false
console.log(isIsogram("Consecutive"));   // false

/*
PROBLEM - 2
    Parity bits are used as very simple checksum to ensure that binary data isn't corrupted during transit. Here's how they work:

    If a binary string has an odd number of 1's, the parity bit is a 1.
    If a binary string has an even number of 1's, the parity bit is a 0.
    The parity bit is appended to the end of the binary string.

    Create a function 'validateBinary' that validates whether a binary string is valid, using parity bits.

    Explanation:
        validateBinary("10110010") ➞ true

        The last digit is the parity bit.
        0 is the last digit.
        0 means that there should be an even number of 1's.
        There are four 1's.
        Return true.

*/
// uisng 2 for loop
function validateBinary(bits) {
    if (bits.charAt(bits.length - 1) == 0) {
        let ans = bits.split('');
        let count = 0;
        for (let i = 0; i < ans.length - 1; i++) {
            if (ans[i] == 1) count++;
        }
        return count % 2 == 0;
    }
    if (bits.charAt(bits.length - 1) == 1) {
        let ans = bits.split('');
        let count = 0;
        for (let i = 0; i < ans.length - 1; i++) {
            if (ans[i] == 1) count++;
        }
        return count % 2 != 0;
    }
}

// second

function validateBinary(bits) {
    let ans = bits.split("");
    let count = 0;
    for (let i = 0; i < ans.length - 1; i++) {
        if (ans[i] == 1) count++;
    } if (bits.charAt(bits.length - 1) == 0) {
        return count % 2 == 0;
    } else {
        return count % 2 != 0;
    }
}

// third solution

function validateBinary(bits) {
    let count = 0;
    for (let i = 0; i < bits.length - 1; i++) {
        if (bits[i] == 1) count++;
    } if (bits[bits.length - 1] == 0) {
        return count % 2 == 0;
    } else {
        return count % 2 != 0;
    }
}
// fourth

function validateBinary(bits) {
    return Array.from(bits).filter((x) => x ==1).length % 2 ==0;
}



console.log(validateBinary("00111101")) // true
console.log(validateBinary("00101101")) // true
console.log(validateBinary("11000000")) // true
console.log(validateBinary("11000001")) // false

/*
PROBLEM - 3

    You will be given a string of characters containing only the following characters: "(", ")", ":"
    Create a function that returns a number based on the number of sad and happy smiley faces there is.
        * The happy faces :) and (: are worth 1.
        * The sad faces :( and ): are worth -1.
        * Invalid faces are worth 0.
    EXPLANATION :
            happinessNumber(":):(") ➞ -1
                The first 2 characters are :)        +1      Total: 1
                The next 2 are ):                    -1      Total: 0
                The last 2 are :(                    -1      Total: -1
*/

function happinessNumber(smilies) {
    let arr = smilies.split('');
    let count = 0;
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] + arr[i + 1] == ':)') count += 1;

        if (arr[i] + arr[i + 1] == '(:') count += 1;

        if (arr[i] + arr[i + 1] == '):') count -= 1;

        if (arr[i] + arr[i + 1] == ':(') count -= 1;
    }
    return count;
}

// console.log(happinessNumber(":):(")) // -1
// console.log(happinessNumber("(:)"))  //  2
// console.log(happinessNumber("::::")) //  0
// console.log(happinessNumber("(:):(:)(?):(:))")) // ?=>false
