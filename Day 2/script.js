// // ----->>> toFixed()
function printAmount() {
	console.log( amount.toFixed( 2 ) );
}

var amount = 99.99;

printAmount(); // "99.99"

amount = amount * 2;

printAmount(); // "199.98"


// //  Values & Types
// As we asserted in Chapter 1, JavaScript has typed values, not typed variables. The following built-in types are available:

// string
// number
// boolean
// null and undefined
// object
// symbol (new to ES6)

// // Truthy & Falsy
// In Chapter 1, we briefly mentioned the "truthy" and "falsy" nature of values: when a non-boolean value is coerced to a boolean, does it become true or false, respectively?

// The specific list of "falsy" values in JavaScript is as follows:

// "" (empty string)
// 0, -0, NaN (invalid number)
// null, undefined
// false
// Any value that's not on this "falsy" list is "truthy." Here are some examples of those:

// "hello"
// 42
// true
// [ ], [ 1, "2", 3 ] (arrays)
// { }, { a: 42 } (objects)
// function foo() { .. } (functions)


// IMP  ==>>>
// For example, arrays are by default coerced to strings by simply joining all the values with commas (,) in between. You might think that two arrays with the same contents would be == equal, but they're not:

var a = [1,2,3];
var b = [1,2,3];
var c = "1,2,3";

// a == c;		// true
console.log(a == c)
// b == c;		// true
console.log(b == c)
// a==b
console.log( a == b );	// false

