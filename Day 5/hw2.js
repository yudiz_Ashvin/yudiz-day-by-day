/*
PROBLEM - 2
Create a function that returns an array that expands by 1 from 1 to the value of the input, and then reduces back to 1. Items in the arrays will be the same as the length of the arrays.

EXAMPLES ::
diamondArrays(1) ➞ [[1]]
diamondArrays(2) ➞ [[1], [2, 2], [1]]
diamondArrays(5) ➞ [[1], [2, 2], [3, 3, 3], [4, 4, 4, 4], [5, 5, 5, 5, 5], [4, 4, 4, 4], [3, 3, 3], [2, 2], [1]]
*/
// function diamondArrays(x) {
//     let sol = [];
//     for (let i = 1; i <= x; i++) {
//       sol.push(Array.from({ length: i }, () => i));
//     }
//     let temp = [...sol];
//     temp.pop();
//     return [...temp, ...sol.reverse()];
//   }
//   diamondArrays(1)
//   diamondArrays(2)
//   diamondArrays(5)

// solution

function diamondArrays(num) {
  const arr = [];
  let toDecrement = false;
  let counter = 0;

  for(let i=0; i< 2* num - 1;i++){
    toDecrement ? arr.push(Array( --counter).fill(counter)) : arr.push(Array(++counter).fill(counter));
    if(i>= num -1) toDecrement = true;
  }
  return arr;
}

console.log(diamondArrays(5));