//my solution

// let n = prompt("Enter number");

// let string = "";
// for (let i = 1; i <=n; i++) {

//   for (let j = 0; j < n - i; j++) {
//     string += " ";
//   }
 
//   for (let k = 0; k < 2 * i - 1; k++) {
//     string += String.fromCharCode(k + 65);
//   }
//   string += "\n";
// }
// console.log(string);

function drawTree(height){
  const width = 2 * height - 1;
  let str = '';
  let curr = 65;

  for(let i =0; i< height; ++i){
    for(let j = 0; j< width; ++j){
      const char = j < height - 1 - i || j > height - 1 + i ? ' ' : String.fromCharCode(curr++);
      str += char;
      curr = curr > 90 ? 65 : curr;
    }
    str += '\n';
    curr = 65;
  }
  return str;
} 
console.log(drawTree(50))

