const { createServer } = require('http');

const port = 2**10; // [0 - 655536) // [2^10 - 2^16)

const requestListener = function (req, res) {
    let message = 'Welcome xyz';

    res.setHeader('Content-Type', 'application/json');
    res.writeHead(200);

    res.end(JSON.stringify({ message }));
};

const server = createServer(requestListener);

server.listen(port, () => {
    console.log(`Server is running on http://localhost:${port}`);
});
