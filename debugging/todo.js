// vscode :> continue/next/step in/step out/ restart / stop
// vscode :> log_point, hit, and conditional breakpoint

// 
// node --inspect server.js  // - for attach 
// node --inspect-brk server.js // - set breakpoint on 1st line.

// launch.json
// launch vs attach

// configure with package.json






















































/* launch.json
{
    // Use IntelliSense to learn about possible attributes.
    // Hover to view descriptions of existing attributes.
    // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
    "version": "0.2.0",
    "configurations": [
        {
            "name": "Attach",
            "port": 9229,
            "request": "attach",
            "skipFiles": [
                "<node_internals>/**"
            ],
            "type": "pwa-node"
        },
        {
            "type": "pwa-node",
            "request": "launch",
            "name": "Launch Program",
            "skipFiles": [
                "<node_internals>/**"
            ],
            "program": "${workspaceFolder}/server.js"
        }
    ]
}
*/