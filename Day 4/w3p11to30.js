//11.

let temp;
prompt("Enter temperature");
//C = (5/9) * (F - 32)
function cToF(temp){
    // const cTemp = temp;
    const cToFar = temp * 9/ 5 +32;
    const total = `${temp} is ${cToFar}`;
    console.log(total)
}
function fToC(temp){
    // const fTemp = temp;
    const fToCel = (temp - 32) * 5/9;
    const total = `${temp} is ${fToCel}`;
    console.log(total)
}
cToF(temp);
fToC(temp);

// Convert temperatures to and from celsius, fahrenheit

function cToF(celsius) 
{
  const cTemp = celsius;
  const cToFahr = cTemp * 9 / 5 + 32;
  const message = `${cTemp}\xB0C is ${cToFahr} \xB0F.`;
    console.log(message);
}

function fToC(fahrenheit) 
{
  const fTemp = fahrenheit;
  const fToCel = (fTemp - 32) * 5 / 9;
  const message = `${fTemp}\xB0F is ${fToCel}\xB0C.`;
    console.log(message);
} 
cToF(60);
fToC(45);

// 12. Write a JavaScript program to get
    // the website URL (loading page)

alert(document.URL);
// 13 
// defination:-
//             Write a JavaScript exercise to create a
//             variable using a user-defined name.

// 14 
// Get the extension of a filename
filename = "system.php"
console.log(filename.split('.').pop());
filename = "abc.js"
console.log(filename.split('.').pop());

//15 
function diff(num){

if(num <= 13) {
    console.log(13 - num);
}else {
    console.log((num - 13) * 2);
}
}
diff(32);
diff(11);

//16. 
//    Write a JavaScript program to compute the sum of the 
//    two given integers. If the two values are same, then 
//    returns triple their sum.

function sum(n1,n2){
    if(n1 == n2) {
        console.log(3* (n1 + n2));
    }
   else{
       console.log(n1 + n2)
    }
}
sum(20,10);
sum(10,10);

//21

function string_check(str1) {
    if (str1 === null || str1 === undefined || str1.substring(0, 2) === 'Py') 
    {
      return str1;
    }else{
    return "Py"+str1;
  }
}
  
  console.log(string_check("Python"));
  console.log(string_check("ptort"));
  console.log(string_check("thon"));

//23
//  Create a new string from a given string with the first character 
//   of the given string added at the front and back

  function first_last(str1) 
  {
  if (str1.length <= 1)
  {
    return str1;
  }
  mid_char = str1.substring(1, str1.length - 1);
  return (str1.charAt(str1.length - 1)) + mid_char + str1.charAt(0);
}
console.log(first_last('a'));
console.log(first_last('ab'));
console.log(first_last('abc'));

//24
//  Create a new string from a given string with the first 
//  character of the given string added at the front and back


function front_back(str)
{
  first = str.substring(0,1);
  return first + str + first;
}
console.log(front_back('a'));
console.log(front_back('ab'));
console.log(front_back('abc'));

// 25. 
//    Write a JavaScript program to check whether a given 
//     positive number is a multiple of 3 or a multiple of 7.

function multi(num) {
  if(num %3 == 0 || num%7 ==0) {
    return true;
  }
  else{
    return false;
  }
}
console.log(multi(23));
console.log(multi(12));
console.log(multi(14));
// console.log(multi(10));
// console.log(multi(11));

//26 Write a JavaScript program to create a new string 
//   from a given string taking the last 3 characters and added 
//   at both the front and back. The string length must be 3 or 
//   more. 

function front_back3(str)
 {
  if (str.length>=3)
   {
  //  str_len = 3;
  back = str.substring(str.length-3);
   return back + str + back;
 }
   else
     return false;
 }
console.log(front_back3("abc"));
// console.log(front_back3("ab"));
// console.log(front_back3("abcd"));

// 27. 
// Write a JavaScript program to check whether a string starts
//  with 'Java' and false otherwise. 
function check(str) {
  if(str.length < 4){
    return false;
  }
 
    ans = str.substring(0,4);// aaya variable declare kyra vagar pn output aave che error ny aav ti a km??
    if(ans  == 'Java'){
      return true;
    }else{
      return false;
    }
  }
  console.log(check("kavascrip"))


// 28. Write a JavaScript program to check whether two given integer values are in the range 50..99 (inclusive). 
//     Return true if either of them are in the said range.

function check_numbers(x, y) 
  {
  if ((x >= 50 && x <= 99) || (y >= 50 && y <= 99))
  {
    return true;
  } 
  else 
  {
    return false;
  }
}

console.log(check_numbers(12, 101));
// console.log(check_numbers(52, 80));
// console.log(check_numbers(15, 99));

// 29.
// Write a JavaScript program to check whether three given 
// integer values are in the range 50..99 (inclusive). 
// Return true if one or more of them are in the said range.

function check_three_nums(x, y, z) 
{
  return (x >= 50 && x <= 99) || (y >= 50 && y <= 99) || (z >= 50 && z <= 99);
}

// 30 
//  Write a JavaScript program to check whether a string 
//   "Script" presents at 5th (index 4) position in a given string, 
//   if "Script" presents in the string return the string without 
//   "Script" otherwise return the original one.

function check_script(str)
{
  if (str.length < 6) {
    return str;
  }
  let result_str = str;
    
  if (str.substring(10, 4) == 'Script') 
    {
    
   result_str = str.substring(0, 4) + str.substring(10,str.length);
      
  }
  return result_str;
}

console.log(check_script("JavaScript"));
console.log(check_script("CoffeeScript"));
