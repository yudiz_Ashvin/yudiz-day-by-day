// let 
// const

// const person ={
//     name: 'ashvi',
//     walk() {},
//     talk() {}
// }
// person.walk();


// const person = {
//     name:'ashvin',
//      walk(){
//         console.log( this );
//     },
// };

// person.walk();

// const walk = person.walk;
// console.log(walk);

//====
// Default Parameter Values

// function foo(x,y) {
//     x = x || 11;
//     y = y || 31;
// console.log( x + y ); }
// foo(); // 42
// foo( 5, 6 );// 11
// foo( 5 );// 36
// foo( null, 6 );// 17

// ===============
