// The literal syntax for an object looks like this:
// var myObj = { key: value
// // ...
// };// ak sathe value add kri saki
// // The constructed form looks like this:
// var myObj = new Object(); myObj.key = value;
// one by one krvi pade


// It’s a common misstatement that “everything in JavaScript is an object.” This is clearly not true.
//------------
// var myObject = { 
//     a:2
// };
// Object.getOwnPropertyDescriptor( myObject, "a" );

// console.log(Object.getOwnPropertyDescriptor( myObject, "a" ));
// // { value: 2, writable: true, enumerable: true, configurable: true }

//-------------
// var myObject = { };
// Object.defineProperty( myObject,"a",{ enumerable: true, value: 2 }
// );
// Object.defineProperty( myObject,"b",{ enumerable: false, value: 3 }
// );
// console.log(myObject.propertyIsEnumerable( "a" ));  // true
// console.log(myObject.propertyIsEnumerable( "b" ));  // false 
// console.log(Object.keys( myObject )); // ["a"]
// console.log(Object.getOwnPropertyNames( myObject )); // ["a", "b"]

// function foo() {
//     console.log( this.a );
//    }
//    var a = 2;
//    var o = { 
//        a: 3, 
//        foo: foo 
//     };
//    var p = { 
//        a: 4 
//     };
//    o.foo(); // 3
//    p.foo = o.foo; // 2


// var anotherObject = { 
//     a:2
// };
// // create an object linked to `anotherObject`
// var myObject = Object.create( anotherObject ); 
// console.log(myObject.a); // 2
// console.log(anotherObject.a);

// var myObject = { a:2
// };
// myObject.a; // 2


// var anotherObject = { a:2
// };
// // create an object linked to `anotherObject`
// var myObject = Object.create( anotherObject ); 
// myObject.a; // 2


//=============

// var anotherObject = { 
//     a:2
// };
// // create an object linked to `anotherObject`
// var myObject = Object.create( anotherObject );
// for (var k in myObject) { 
//     console.log("found: " + k);
// }
// // found: a
// console.log("a" in myObject); // true
//====
// function NothingSpecial() { 
//     console.log( "Don't mind me!" );
// }
// var a = new NothingSpecial();
// // "Don't mind me!" 
// console.log(a);  // {}

//===========

// function Foo(name) { 
//     this.name = name;
// }
// Foo.prototype.myName = function() { 
//     return this.name;
// }; 
// var a=new Foo("a");
// var b=new Foo("b"); 
// a.myName(); // "a"
// b.myName(); // "b"
function foo() {
    console.log( this.a );
   }
   var a = 2;
   var o = { a: 3, foo: foo };
   var p = { a: 4 };
   o.foo(); // 3
   (p.foo = o.foo)(); // 2
