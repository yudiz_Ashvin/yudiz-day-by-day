// creat object using dot

var emp1 = {};
emp1.fname ='ashvin';
emp1.lname = 'vanol'
emp1.city = 'Jamnagar';
emp1.age = '20'

var emp2 = {};
emp2.fname ='uday';
emp2.lname = 'nalipara'
emp2.city = 'rajkot';
emp2.age = '22'

// console.log(emp1);
// console.log(emp2);

//========================
// creat object using function

var emp1 = {};
emp1.fname ='ashvin';
emp1.lname = 'vanol'
emp1.city = 'Jamnagar';
emp1.age = 20

var emp2 = {};
emp2.fname ='uday';
emp2.lname = 'nalipara'
emp2.city = 'rajkot';
emp2.age = 22

function emp(fname,lname,city,age){
    var newEmp = {};
    newEmp.fname =fname;
    newEmp.lname = lname;
    newEmp.city = city;
    newEmp.age = age;
    return newEmp;
}

var emp3 = emp("ajay","vanol","jama",19)

// console.log(emp1);
// console.log(emp2);
// console.log(emp3);
//========================

// creat object using constructor

var emp1 = {};
emp1.fname ='ashvin';
emp1.lname = 'vanol'
emp1.city = 'Jamnagar';
emp1.age = 20

var emp2 = {};
emp2.fname ='uday';
emp2.lname = 'nalipara'
emp2.city = 'rajkot';
emp2.age = 22

function emp(fname,lname,city,age){
    // var newEmp = {};
    this.fname =fname;
    this.lname = lname;
    this.city = city;
    this.age = age;
    // return newEmp;
}

var emp3 = emp("ajay","vanol","jama",19)
var emp4 = new emp("rahul","gohil","jamtara",22)


// console.log(emp1);
// console.log(emp2);
// // console.log(emp3);
// console.log(emp4);

//========================

/* 
if you creat a object using function you have no need to add new keyword

but you can add a new keyword the output not change
*/
function bicycle(cadence,speed,gear){
    var newBicycle = {};
    newBicycle.cadence =cadence;
    newBicycle.speed = speed;
    newBicycle.gear = gear;
    return newBicycle;
}

var cycle1 = bicycle(50,20,5)
var cycle2 = new bicycle(20,5,1)

// console.log(cycle1);
// console.log(cycle2);

/* 
1) if you creat a object using constructors function you need to add new key word

2) identification of constructor function ic starting with capital letters.

3) without new it's give an undefind 

*/
function Bicycle(cadence,speed,gear){
    // var newBicycle = {};
    this.cadence =cadence;
    this.speed = speed;
    this.gear = gear;
    // return newBicycle;
}

var bcycle1 = Bicycle(50,20,5)
var bcycle2 = new Bicycle(20,5,1)

// console.log(bcycle1); //undefined
// console.log(bcycle2); 
// //Bicycle { cadence: 20, speed: 5, gear: 1 }

//========================

// prototypes in js

