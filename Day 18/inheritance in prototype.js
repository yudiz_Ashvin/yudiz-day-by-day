const a = {
    name: 'ash',
    age: { dob: 12},
}
// const b={};
// const b = Object.assign({},a);
// const b = {...a};
const b = JSON.parse(JSON.stringify(a))

// b.name = "hitesha";
b.age.dob = 13;
// console.log(b.name);
console.log(a.age.dob);
console.log(b.age.dob);

let obj = {
    name: "ashvin",
    city: "jamnagar",
    info: function(){
        console.log(this.name + "from " + this.city);
    }
}

let obj2 ={
    name: "ajay"
}
obj2 = obj;
console.log(obj.name);
console.log(obj2.name);
console.log(obj.city);
console.log(obj2.city);

// inheritance

//constructor
function Employee(name,salary, experience){
    this.name = name;
    this.salary =salary;
    this.experience = experience;
}
let ashvin= new Employee("ashvin",2000,0);
console.log(ashvin);


function WebDev(name,salary, experience,language){
    Employee.call(this,name,salary,experience);
    this.language=language;
}
let ajay = new WebDev("ajay", 8000,5,"react")
console.log(ajay);

function city(name,salary, experience,language,city){
    WebDev.call(this,name,salary,experience,language);
    this.city = city;
}
let uday = new city("ajay", 8000,5,"react","rajkot")
console.log(uday);


