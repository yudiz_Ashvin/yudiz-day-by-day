function Shape(h, w) {
    this.h = h;
    this.w = w;
}
Shape.prototype.getHeight = function () {
    return this.h;
};
Shape.prototype.getWidth = function () {
    return this.w;
};

// const n1 = new Shape(20,30)
// console.log(n1);

function Rectangle(h, w) {
    Shape.call(this, h, w);
}

Object.setPrototypeOf(Rectangle.prototype, Shape.prototype); // - linking

// // Rectangle.prototype = Object.create(Shape.prototype); // -linking
// // Rectangle.prototype.constructor = Rectangle;

// // extends
Rectangle.prototype.area = function () {
    return this.h * this.w;
};

// const r1 = new Rectangle(10, 20);
// console.log(r1.area(), r1.getHeight(), r1.getWidth());


function Triangle(h, w) {
    Shape.call(this, h, w);
}
Object.setPrototypeOf(Triangle.prototype, Shape.prototype);


function RoundedRectangle(h, w, r) {
    Shape.call(this, h, w);
    this.r = r;
}

Object.setPrototypeOf(RoundedRectangle.prototype, Rectangle.prototype);
RoundedRectangle.prototype.getRadius = function () {
    return this.r;
};
RoundedRectangle.prototype.area = function () {
    return this.h * this.w + this.r;
};
RoundedRectangle.prototype.area2 = function () {
    return Rectangle.prototype.area.call(this);
};

const r2 = new RoundedRectangle(10, 20, 5);
console.log(r2.area(), r2.area2(), r2.getHeight(), r2.getWidth(), r2.getRadius());