function intro() {
    class Person {
        constructor(name, age, city) {
            this.name = name;
            this.age = age;
            this.city = city;
            // this.my = function my(){
            //     return `hi, I'm ${this.name}`
            // }
        }
        introduce() {
            return `hi, I'm ${this.name}`;
        }
    }

    Person.prototype.canVote = function () {
        return this.age > 18;
    };

    const anuj = new Person('anuj', 60, 'Jamtara');
    const bipin = new Person('bipin', 10, 'Mumbai');

    console.log(anuj);
    console.log(bipin);
    console.log(anuj.canVote());
    console.log(bipin.canVote());
    // console.log(anuj.introduce());?????

    // * NOTE : class can't be used before declaration.
    // static
}
// intro();

function accessModifier() {
    class RectanglePublic {
        height = 0;
        width;
        constructor(height, width) {
            this.height = height;
            this.width = width;
        }
        // width1 = 0;
        // area =0;
        get area() {
            return this.height * this.width;
        }
        meta() {
            return { height: this.height, width: this.width, area: this.area };
        }
    }
    class RectanglePrivate {
        #height = 0;
        #width;
        constructor(height, width) {
            this.#height = height;
            this.#width = width;
        }
        get #area() {
            return this.#height * this.#width;
        }
        meta() {
            return { height: this.#height, width: this.#width, area: this.#area };
        }
    }

    var r1 = new RectanglePublic(10, 20);
    var r2 = new RectanglePrivate(100, 20);
    console.log(r1, r2);
    console.log(r1.meta(), r2.meta());
}
accessModifier();

function inheritance() {
    class Vehicle {
        constructor(licensePlate) {
            this.licensePlate = licensePlate;
        }
        getVehicleNumber() {
            return this.licensePlate;
        }
    }

    class TwoWheeler extends Vehicle {
        constructor(licensePlate, wheel) {
            super(licensePlate);
            this.wheel = wheel;
        }
        getNumWheels() {
            return this.wheel;
        }
    }

    class Scooter extends TwoWheeler {
        constructor(licensePlate, wheel, type) {
            super(licensePlate, wheel);
            this.type = type;
        }
        getType() {
            return this.type;
        }
    }

    class Bike extends TwoWheeler {
        constructor(licensePlate, wheel, type, gear) {
            super(licensePlate, wheel);
            this.type = type;
            this.gear = gear;
        }
        getType() {
            return this.type;
        }
        getTGear() {
            return this.gear;
        }

        //method overriding
        // getVehicleNumber() {
        //     return this.licensePlate.toLowerCase();
        // }
        getVehicleNumberFromParent() {
            return Vehicle.prototype.getVehicleNumber.call(this);
        }
    }

    var b1 = new Bike('JH21F-7785', 2, 'petrol', 5);
    console.log(b1);
    // console.log(b1.getVehicleNumber());
    // console.log(b1.getVehicleNumberFromParent());
}

// inheritance();
