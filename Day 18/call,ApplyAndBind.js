let hello = {
    firstname:'ashvin',
    lastname:'vanol',
    // printFullName: function(){
    //     console.log(this.firstname +" "+ this.lastname);
    // }
}
let printFullName = function(hometown, state){
    console.log(this.firstname +" "+ this.lastname + ", " + hometown+ " ," + state);
}
printFullName.call(hello, "jamnagar", 'gujarat')
// console.log(name);

let name2 = {
    firstname: 'ajay',
    lastname: 'vanol'
}
//call
printFullName.call(name2,'vadodra', 'gujarat');
//apply
printFullName.apply(name2,['vadodra', 'gujarat']);
// bind its giv a copy  witch can invoca later

let myname = printFullName.bind(name2,'vadodra', 'gujarat')
// console.log(myname); //return an function
myname();