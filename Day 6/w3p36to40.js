// 36. Write a JavaScript program to check whether the last
//     digit of the three given positive integers is same.

// function last_digit(x, y, z) {
//   var check1 = x.toString();
//   var A = check1.charAt(check1.length - 1);
//   var check2 = y.toString();
//   var B = check2.charAt(check2.length - 1);
//   var check3 = z.toString();
//   var C = check3.charAt(check3.length - 1);
//   if( (x>0) &&(y>0) && (z>0)){
//     return (A === B && B === C);
//   }else{
//       return false;
//   }
// }

// console.log(last_digit(10,20,30));
// console.log(last_digit(-20, 30, 400));


// OR

function last_digit(x, y, z)
{
    x %= 10;
    y %= 10;
    z %= 10;
  if ((x > 0) && y > 0 && z > 0)
    {
     return (x == y && y == z && x == z);
   }
  else
    return false;
}
// console.log(last_digit(135, -231, 31));
// console.log(last_digit(16, 36, 146));

// 37. Write a JavaScript program to create new string with
//     first 3 characters are in lower case from a given string.
//     If the string length is less than 3 convert all the
//     characters in upper case.

     function upper_lower(str){
         if(str.length > 3){
             front = (str.substring(0,3)).toLowerCase();
             return front + str.substring(3,str.length);
         }
         else{
             return false;
         }
     }
    //  console.log(upper_lower("PYThon"));
    //  console.log(upper_lower("Py"));
    //  console.log(upper_lower("JAVAScript"));



// 38. Write a JavaScript program to check the total marks of
//     a student in various examinations. The student will get
//     A+ grade if the total marks are in the range 89..100
//    inclusive, if the examination is "Final-exam." the student
//    will get A+ grade and total marks must be greater than or
//    equal to 90. Return true if the student get A+ grade or
//    false otherwise.

// function exam_status(totmarks,is_exam)
//   {
//   if (is_exam) {
//     return totmarks >= 90;
//   }
//  return (totmarks >= 89 && totmarks <= 100);
//  }

 function exam_status(totmarks){
     if(totmarks>= 90 && totmarks <=100) {
         return true;
     }
     else{
         return false;
     }
 }

// console.log(exam_status(70));
// console.log(exam_status(80));
// console.log(exam_status(90));

// 39. Write a JavaScript program to compute the sum of the
//     two given integers, If the sum is in the range 50..80
//     return 65 other wise return 80.  


function sum(n1,n2){
    if((n1+n2) >=50 && (n1+n2) <=80){
        return 65;
    }
    else{
        return 80;
    }
}
// console.log(sum(55,75));
// console.log(sum(30,20));


// 40. Write a JavaScript program to check from two given
//    integers whether one of them is 8 or their sum or
//    difference is 8.

function check8(x, y) {
    if (x == 8 || y == 8) {
      return true;
    }
    if (x + y == 8 || Math.abs(x - y) == 8)
    {
      return true;
    }
  
    return false;
  }
  
  console.log(check8(7, 8));
  console.log(check8(16, 8));
  console.log(check8(24, 32));
  console.log(check8(17, 18));