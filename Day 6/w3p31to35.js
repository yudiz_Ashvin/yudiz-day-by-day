// 31. Write a JavaScript program to find the largest of
//     three given integers.

function large(a, b, c) {
  let max_value = 0;
  if (a > b) {
    max_value = a;
  } else {
    max_value = b;
  }
  if (max_value < c) {
    max_value = c;
  }
  console.log(max_value);
}
// large(10,20,30);

// 32. Write a JavaScript program to find a value which is
//     nearest to 100 from two different given integer values.

function near(x, y) {
  if (x != y) {
    x1 = Math.abs(x - 100);
    y1 = Math.abs(y - 100);
    if (x1 > y1) {
      console.log(y);
    }
    if (x1 < y1) {
      console.log(x);
    }
    return 0;
  } else {
    console.log(false);
  }
}
// near(90,89)
// near(90,90)

// 33. Write a JavaScript program to check whether two numbers
//     are in range 40..60 or in the range 70..100 inclusive.

function range(x, y) {
  if (
    (x >= 40 && x <= 60 && y >= 40 && y <= 60) ||
    (x >= 70 && x <= 100 && y >= 70 && y <= 100)
  ) {
    console.log("true");
  } else {
    console.log("false");
  }
}
// range(20, 25);

// 34. Write a JavaScript program to find the larger number
//     from the two given positive integers, the two numbers are
//     in the range 40..60 inclusive.

function max(x, y) {
  if (x >= 40 && x <= 60 && y >= 40 && y <= 60) {
    if (x === y) {
      console.log("Numbers are the same");
    } else if (x > y) {
      console.log(x);
    } else {
      console.log(y);
    }
  } else {
    console.log("Numbers don't fit in range");
  }
}
// max(45,60)
// 35. Write a program to check whether a specified character exists
// within the 2nd to 4th position in a given string.

function check_char(str1, char) {
  ctr = 0;
  for (let i = 0; i < str1.length; i++) {
    if (str1.charAt(i) == char && i >= 1 && i <= 3) {
      ctr = 1;
      break;
    }
  }
  if (ctr == 1) return true;
  return false;
}
// console.log(check_char("Python", "y"));
// console.log(check_char("JavaScript", "a"));





