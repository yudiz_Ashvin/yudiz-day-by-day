// 81. Write a JavaScript program to add two digits of a given positive integer of length two.

function add(num) {
    return num % 10 + Math.floor(num / 10);
}
// console.log(add(25))
// console.log(add(50))

// 82. Write a JavaScript to add two positive integers without carry. 
function add_two_int_without_carrying(n1, n2) {
    var result = 0,
        x = 1;
    while (n1 > 0 && n2 > 0) {
        result += x * ((n1 + n2) % 10);
        n1 = Math.floor(n1 / 10);
        n2 = Math.floor(n2 / 10);
        x *= 10;
    }
    return result;
}
// console.log(add_two_int_without_carrying(222, 911))
// console.log(add_two_int_without_carrying(200, 900))


// 83. Write a JavaScript to find the longest string from a given array of strings.

function longest_string(str_ara) {
    let max = str_ara[0].length;
    str_ara.map(v => max = Math.max(max, v.length));
    result = str_ara.filter(v => v.length == max);
    return result;
}

function longest_string(str_ara) {
    return str_ara.reduce((init, curr) => (init.length < curr.length ? curr : init)
    );
}

// console.log(longest_string(['a', 'aa', 'aaa',
//     'aaaaa', 'aaaa']))


// 84. Write a JavaScript to replace each character of a given string by the next one in the English alphabet.
// Note: 'a' will be replace by 'b' or 'z' would be replaced by 'a'.

// first
function replace(str) {
    const all_chars = str.split('');
    for (let i = 0; i < all_chars.length; i++) {
        let n = all_chars[i].charCodeAt() - 'a'.charCodeAt();
        n = (n + 1) % 26;
        all_chars[i] = String.fromCharCode(n + 'a'.charCodeAt());
    }
    return all_chars.join('');
}

// second
function replace(str) {
    return str.split('').map(char =>
        char === ' ' ? ' ' :
            char === 'z' ? 'a' :
                char === 'Z' ? 'A' :
                    String.fromCharCode(char.charCodeAt(char) + 1)
    ).join('')
}
// console.log(replace("ciao")); // first
// console.log(replace("abcDxyz")); // second

// 85. Write a JavaScript code to divide a given array of positive integers into two parts. First element goes to first part, second element goes to second part, and third element goes to first part and so on. Now compute the sum of two parts and store into an array of size two. 

function sum(num) {
    let first = 0;
    let second = 0;
    for (let i = 0; i < num.length; i++) {
        if (i % 2 == 0) first += num[i];
        else second += num[i]

    }
    return [first, second]
}
// console.log(sum([1,2,3,4,5,6,7,8,9]));
// console.log(sum([1, 3, 6, 2, 5, 10]))


// 86. Write a JavaScript program to find the types of a given angle.
// Types of angles:
// Acute angle: An angle between 0 and 90 degrees.
// Right angle: An 90 degree angle.
// Obtuse angle: An angle between 90 and 180 degrees.
// Straight angle: A 180 degree angle.
function angle_Type(angle) {
    if (angle < 90) {
        return "Acute angle.";
    } else if (angle === 90) {
        return "Right angle.";
    } else if (angle < 180) {
        return "Obtuse angle.";
    }
    return "Straight angle.";
}

//   console.log(angle_Type(47))
//   console.log(angle_Type(90))
//   console.log(angle_Type(145))
//   console.log(angle_Type(180))

// 87. Write a JavaScript program to check whether two arrays of integers of same length are similar or not. The arrays will be similar if one array can be obtained from another array by swapping at most one pair of elements. 
function compare(arra1, arra2) {

    for(var i = 0; i < arra1.length; i++) {
      for(var j = i; j < arra1.length; j++) {
        var result = true,
          temp = arra1[i];
        arra1[i] = arra1[j];
        arra1[j] = temp;
        for(var k = 0; k < arra1.length; k++) {
          if(arra1[k] !== arra2[k]) {
            result = false;
            break;
          }
        }
        if(result) {
          return true;
        }
        arra1[j] = arra1[i];
        arra1[i] = temp;
      }
    }
    return false;
  }
  //second
function compare(arr1, arr2) {
    if (arr1.length != arr2.length) return false;
    for (let i = 0; i < arr1.length; i++) {
        if (arr1[i] != arr2[i]) return false;
    }
    return true;
}
// console.log(compare([10,20,30], [10,20,30]))
// console.log(compare([10,20,30], [30,10,20]))
// console.log(compare([10,20,30,40], [10,30,20,40]))

// 88. Write a JavaScript program to check whether two given integers are similar or not, if a given divisor divides both integers and it does not divide either. 

function check(a,b,c){
    return a%c === b%c
}
// console.log(check(10, 25, 5))
// console.log(check(10, 20, 5))
// console.log(check(10, 20, 4))

// 89. Write a JavaScript program to check whether it is possible to replace $ in a given expression x $ y = z with one of the four signs +, -, * or / to obtain a correct expression. 
// For example x = 10, y = 30 and z = 300, we can replace $ with a multiple operator (*) to obtain x * y = z

function add(x,y,z){
    if(x+y == z) return true;
    if(x-y == z) return true;
    if(x*y == z) return true;
    if(x/y == z) return true;
    return false;
}
// console.log(add(10, 25, 35))
// console.log(add(10, 25, 250))
// console.log(add(30, 25, 5))
// console.log(add(100, 25, 4.0))
// console.log(add(100, -25, 25))


// 90. Write a JavaScript program to find the kth greatest element of a given array of integers 

function find(arr, k){
    // let ans = arr.sort();
    return arr.sort()[k-1];
}
// console.log(find([1,2,6,4,5], 3))
// console.log(find([-10,-25,-47,-36,0], 1))

// w3 solution
function Kth_greatest_in_array(arr, k) {

    for (var i = 0; i < k; i++) {
      var max_index = i,
        tmp = arr[i];
  
      for (var j = i + 1; j < arr.length; j++) {
        if (arr[j] > arr[max_index]) {
          max_index = j;
        }
      }
  
      arr[i] = arr[max_index];
      arr[max_index] = tmp;
    }
  
    return arr[k - 1];
  }
  
//   console.log(Kth_greatest_in_array([1,2,6,4,5], 3))
//   console.log(Kth_greatest_in_array([-10,-25,-47,-36,0], 1))
  
