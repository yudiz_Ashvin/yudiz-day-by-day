    // on / addEventListener method
    // off / removeEventListener method
    // once method
    // emit method
    // rawListeners method
    // listenerCount methodconst 


const event = require("events")

const emitter = new event()

// emitter.on("first", (message) => {
//   // emitter.once("message", (message) => {
//   console.log("subsribed", message)
// })

// emitter.emit("first", 123)

// emitter.on("first", (message) => {
//   // emitter.once("message", (message) => {
//   console.log("subsribed", message)
// })


// emitter.emit("message", 123)
// emitter.emit("message", 123)


// emitter.off('message', (msg, status) => {
// console.log('mes print', msg, status)
// })
// emitter.emit("message", "ok", 500) //noop

// console.log(emitter.listenerCount('message')); //0



// emitter.on('error', (err) => {
//   console.error('whoops! there was an error');
// });
// emitter.emit('error', new Error('whoops!'));

//=====

// Only do this once so we don't loop forever
// emitter.once('newListener', (event, listener) => {
//   if (event === 'event') {
//     // Insert a new listener in front
//     emitter.on('event', () => {
//       console.log('B');
//     });
//   }
// });
// emitter.on('event', () => {
//   console.log('A');
// });
// emitter.emit('event');
// Prints:
//   B
//   A

//=====

// emitter.on('login', (name,email,pass,mono)=>{
// console.log('first name:', name ,email,pass,mono);
// })
// emitter.emit('login', {name:"ashvin"},{email:"ashvin@gmail.com"}, {password: "Ashvin@123"}, {MoNo:6353438566})


const fs = require('fs')

const logEvent =  (msg)=>{
  // const dateTime = `${(new Date(), 'new date')}`
  const logItem = `${new Date}\n${msg}`
  // console.log(logItem);
  // console.log('hello', msg);
   try{
        fs.writeFileSync('./time .txt', logItem, {flag:'a'})
      }catch(e){
        console.log(e);
    }
}
emitter.on('log', (msg) => logEvent(msg));

emitter.emit('log', 'log  event amitted! first time \n')

setTimeout(()=>{
  emitter.emit('log', 'log  event amitted! second time \n')
},2000)

setTimeout(()=>{
  emitter.emit('log', 'log  event amitted! thirdtime\n\n')
},5000)

//===============

// emitter.on('myEvent', (data) => {
//   console.log(data);
// });

// console.log('Statement A');
// emitter.emit('myEvent', 'Statement B');
// console.log("Statement C");

//=============

// console.log(emitter.eventNames());