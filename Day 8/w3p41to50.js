// 41. Write a JavaScript program to check three given numbers, if the three numbers are same return 30 otherwise return 20 and if two numbers are same return 40.

function same(x, y, z) {
    if (x == y == z) {
        return 30;
    }
    if (x == y || y == z || z == x) {
        return 40;
    }
    return 20;
}
// console.log(same(1,1,1))
// console.log(same(2,2,22))
// console.log(same(3,33,333))

// 42. Write a JavaScript program to check whether three given numbers are increasing in strict mode or in soft mode.
// Note: Strict mode -> 10, 15, 31 : Soft mode -> 24, 22

function increasing(a, b, c) {
    if (a < b && b < c) {
        return "strict mode";
    }
    else if (b < c) {
        return "Soft mode";
    }
    else {
        return "Undefinded";
    }
}
// console.log(increasing(10,15,31));
// console.log(increasing(24,22,31));
// console.log(increasing(50,21,15));


// 43. Write a JavaScript program to check from three given numbers (non negative integers) that two or all of them have the same rightmost digit.

function last_digit(x, y, z) {
    if ((x % 10) === (y % 10) || (y % 10) === (z % 10) || (z % 10) === (x % 10)) {
        return true;
    }
    return false;
}
// console.log(last_digit(22,32,42));
// console.log(last_digit(102,302,2));
// console.log(last_digit(20,22,45));

// 44. Write a JavaScript program to check from three given integers that whether a number is greater than or equal to 20 and less than one of the others.
function lessby20_others(x, y, z) {
    return (x >= 20 && (x < y || x < z)) ||
        (y >= 20 && (y < x || y < z)) ||
        (z >= 20 && (z < y || z < x));
}
// console.log(lessby20_others(23, 45, 10));
// console.log(lessby20_others(23, 23, 10));
// console.log(lessby20_others(21, 66, 75));


// 45. Write a JavaScript program to check two given integer values and return true if one of the number is 15 or if their sum or difference is 15. 

function check(a, b) {
    if (a == 15 || b == 15 || (a + b) == 15 || (a - b) == 15) {
        return true;
    }
    return false;
}
// console.log(check(15, 9));
// console.log(check(25, 15));
// console.log(check(7, 8));
// console.log(check(25, 10));
// console.log(check(5, 9));
// console.log(check(7, 9));
// console.log(check(9, 25));


// 46. Write a JavaScript program to check two given non-negative integers that whether one of the number (not both) is multiple of 7 or 11.
function check(x, y) {
    if (x > 0 && y > 0) {
        return ((x % 7 == 0 || y % 7 == 0) || (x % 11 == 0 || y % 11 == 0))
    }
    return false;
}

// console.log(check(-14, 21));
// console.log(check(14, 20));
// console.log(check(16, 20));


// 47. Write a JavaScript program to check whether a given number is presents in the range 40..10000.
// For example 40 presents in 40 and 4000.
function check(x, y, num) {
    if (num < 40 || num > 10000) {
        return false;
    } else if (num >= x && num <= y) {
        return true;
    } else {
        return false;
    }
}
// console.log(check(40, 4000, 45));  
// console.log(check(80, 320, 79));  
// console.log(check(89, 4000, 30));

// 48. Write a JavaScript program to reverse a given string.
function Reverse(str) {
    return str.split("").reverse().join('')
    //str = ashvin
    //str.split=>[ 'a', 's', 'h', 'v', 'i', 'n' ]
    //str.reverse=>[ 'n', 'i', 'v', 'h', 's', 'a' ]
    //str.join('') =>  nivhsa
}
// console.log(Reverse("ashvin"))


// 49. Write a JavaScript program to replace every character in a given string with the character following it in the alphabet.
// function string_reverse(str)
function LetterChanges(text) {
    //https://goo.gl/R8gn7u
    var s = text.split('');
    for (var i = 0; i < s.length; i++) {
        // Caesar cipher
        switch (s[i]) {
            case ' ':
                break;
            case 'z':
                s[i] = 'a';
                break;
            case 'Z':
                s[i] = 'A';
                break;
            default:
                s[i] = String.fromCharCode(1 + s[i].charCodeAt(0));
        }

        // Upper-case vowels
        switch (s[i]) {
            case 'a': case 'e': case 'i': case 'o': case 'u':
                s[i] = s[i].toUpperCase();
        }
    }
    return s.join('');
}
console.log(LetterChanges("python"));
console.log(LetterChanges("W3R"));
console.log(LetterChanges("php"));



// 50. Write a JavaScript program to capitalize the first letter of each word of a given string.
function capital(str){

//whenever a blank space is encountered

const arr = str.split(" ");
// return arr

// loop through each element of the array and capitalize the first letter.

for (var i = 0; i < arr.length; i++) {
    arr[i] = arr[i].charAt(0).toUpperCase() + arr[i].slice(1);
    // The slice() method returns selected elements in an array, as a new array.
}

//Join all the elements of the array back into a string 
//using a blankspace as a separator 
const str2 = arr.join(" ");
return str2
}
console.log(capital('Write a JavaScript program to capitalize the first letter of each word of a given string.'));
// console.log(capital("hello wvwriuafcb"))



