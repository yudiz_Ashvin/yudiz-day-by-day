# Yudiz Day By Day

## [Day 1](https://gitlab.com/yudiz_Ashvin/yudiz-day-by-day/-/tree/main/Day%201)

```
- JavaScript basic

```
## [Day 2](https://gitlab.com/yudiz_Ashvin/yudiz-day-by-day/-/tree/main/Day%202)

```
- w3resource problem
- JavaScript basic

```
## [Day 3](https://gitlab.com/yudiz_Ashvin/yudiz-day-by-day/-/tree/main/Day%203)

```
- w3resource problem
- HTML & CSS basic

```
## [Day 4](https://gitlab.com/yudiz_Ashvin/yudiz-day-by-day/-/tree/main/Day%204)

```
- JavaScript basic  Mcq
- Array 
- w3resource problem

```
## [Day 5](https://gitlab.com/yudiz_Ashvin/yudiz-day-by-day/-/tree/main/Day%205)

```
- w3resource problem
- HW problem
- HTML & CSS
```
## [Day 6](https://gitlab.com/yudiz_Ashvin/yudiz-day-by-day/-/tree/main/Day%206)

```
- w3resource problem
- string

```
## [Day 7](https://gitlab.com/yudiz_Ashvin/yudiz-day-by-day/-/tree/main/Day%207)

```
- Demo project of HTML & CSS
<!-- - [Demo project of HTML & CSS](https://demo-project-yudiz.netlify.app/) -->
- problem 
- Compiler theory
- Hosting

```
## [Day 8](https://gitlab.com/yudiz_Ashvin/yudiz-day-by-day/-/tree/main/Day%208)

```
- w3resource problem
- Hosting
- Lexical Scope
- Closure
```
## [Day 9](https://gitlab.com/yudiz_Ashvin/yudiz-day-by-day/-/tree/main/Day%209)
```
- Var, Let & const
- w3resource problem
- scope & closer MCQ

```
## [Day 10](https://gitlab.com/yudiz_Ashvin/yudiz-day-by-day/-/tree/main/Day%2010)

```
- w3resource problem
- String Problem
- String methods
- Event Loop

```
## [Day 11](https://gitlab.com/yudiz_Ashvin/yudiz-day-by-day/-/tree/main/Day%2011)

```
- w3resource problem
- Execution Context
- call Stack
- callback function

```

## [Day 12](https://gitlab.com/yudiz_Ashvin/yudiz-day-by-day/-/tree/main/Day%2012)

```
- JavaScript String Methods
- w3resource problem
- Promises
- Stack and Queue

```
## [Day 13](https://gitlab.com/yudiz_Ashvin/yudiz-day-by-day/-/tree/main/Day%2013)

```
- JavaScript Array Methods
- w3resource problem
- test

```
## [Day 14](https://gitlab.com/yudiz_Ashvin/yudiz-day-by-day/-/tree/main/Day%2014)

```
- stack problem
- thunk & generator test

```
## [Day 15](https://gitlab.com/yudiz_Ashvin/yudiz-day-by-day/-/tree/main/Day%2015)

```
- w3resource problem
- stack problem
- thunk & generator revision

```
## [Day 16](https://gitlab.com/yudiz_Ashvin/yudiz-day-by-day/-/tree/main/Day%2016)

```
- this and object
- Shallow and Deep copy

```
## [Day 17](https://gitlab.com/yudiz_Ashvin/yudiz-day-by-day/-/tree/main/Day%2017)

```
- Prototype in js
- w3 problem 

```
## [Day 18](https://gitlab.com/yudiz_Ashvin/yudiz-day-by-day/-/tree/main/Day%2018)

```
- class
- prototypal-inheritence
- logical problem( Valid Parentheses )
- call Apply And bind
- test of this and object

```