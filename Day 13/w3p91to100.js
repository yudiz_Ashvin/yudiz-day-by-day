// 91. Write a JavaScript program to find the maximum possible sum of some of its k consecutive numbers (numbers that follow each other in order.) of a given array of positive integers.
function sum(nums, k) {
  let ans = 0;

  for (let i = 0; i < nums.length - k + 1; i++) {
    let sum = 0;
    for (let j = 0; j < k; j++) sum += nums[j + i];
    if (ans < sum) ans = sum;
  }
  return ans;

}
// console.log(sum([10, 20, 30, 40, 60, 70], 3))
// console.log(sum([1, 2, 3, 14, 5], 2))
// console.log(sum([2, 3, 5, 1, 6], 3))
// console.log(sum([9, 3, 5, 1, 7], 2))

// 92. Write a JavaScript program to find the maximal difference between any two adjacent elements of a given array of integers.

function compare(arr) {
  let max = -1;
  let ans;
  for (let i = 0; i < arr.length - 1; i++) {
    ans = Math.abs(arr[i] - arr[i + 1]);
    //  if(ans > max){
    //    max = ans;
    //  }
    max = Math.max(ans, max);
  }
  return max;
}
// console.log(compare([1, 2, 3, 8, 9]))
// console.log(compare([1, 2, 3, 18, 9]))
// console.log(compare([13, 2, 3, 8, 9]))

// 93. Write a JavaScript program to find the maximal difference among all possible pairs of a given array of integers.

function max_difference(arr) {
  var max = arr[0];
  var min = arr[0];
  for (var i = 0; i < arr.length; i++) {
    max = Math.max(max, arr[i]);
    min = Math.min(min, arr[i]);
  }
  return max - min;
}
// console.log(max_difference([1, 2, 3, 8, 9]))
// console.log(max_difference([1, 2, 3, 18, 9]))
// console.log(max_difference([13, 2, 3, 8, 9]))

// 94. Write a JavaScript program to find the number which appears most in a given array of integers. 

function mostAppears(arr) {
  var max_count = 0;
  var ans = 0;
  for (i = 0; i < arr.length; i++) {
    var count_temp = 0;
    for (k = i + 1; k < arr.length; k++) {
      if (arr[i] == arr[k]) {
        count_temp++;
      }
      if (count_temp > max_count) {
        max_count = count_temp;
        ans = arr[i];
      }
    }
  }
  return ans;
}

// console.log(mostAppears([2, 3, 1, 2, 2, 8, 1, 9,2]))

// 95. Write a JavaScript program to replace all the numbers with a specified number of a given array of integers.
function change(arr, oldnum, newnum) {
  for (let i = 0; i < arr.length; i++) {
    if (arr[i] === oldnum) {
      arr[i] = newnum;
    }
  }
  return arr;
}
// console.log(change([1, 2, 3, 2, 2, 8, 1, 9], 9, 5));
// console.log(change([1, 2, 3, 2, 2, 8, 1, 9], 2, 5));


// 96. Write a JavaScript program to compute the sum of absolute differences of consecutive numbers of a given array of integers. 
function diff(arr) {
  let count = 0;
  for (let i = 1; i < arr.length; i++) {
    count += Math.abs(arr[i] - arr[i - 1]);
  }
  return count;
}
// console.log(diff([1, 2, 3, 2, -5]));


// 97. Write a JavaScript program to find the shortest possible string which can create a string to make it a palindrome by adding characters to the end of it.
function buildPalindrome(str) {
  const result = [];

  for (let i = 0; i < str.length; i++) {
    const step = [...str.slice(i)]
    if (str.slice(i) === step.reverse().join('')) {
      return `${str}${result.reverse().join('')}`
    } else {
      result.push(str[i])
    }
    return step

  }

}

// console.log(buildPalindrome("abcddc")) // abcddcba
// console.log(buildPalindrome("1223")) // 1223221
// console.log(buildPalindrome("122")) // 1223221



// 98. Write a JavaScript program to switch case of the minimum possible number of letters to make a given string written in the upper case or in the lower case. 
// Fox example "Write" will be write and "PHp" will be "PHP"

function change(new_str) {
  var x = 0;
  var y = 0;

  for (var i = 0; i < new_str.length; i++) {
    if (/[A-Z]/.test(new_str[i])) {
      x++;
    } else y++;
  }

  if (y > x) return new_str.toLowerCase();
  return new_str.toUpperCase();
}

// console.log(change("Write"))
// console.log(change("PHp"))

// 99. Write a JavaScript program to check whether it is possible to rearrange characters of a given string in such way that it will become equal to another given string.

//first
function compare(str1, str2){
  let fstr = str1.split('').sort().join('');
  let sstr = str2.split('').sort().join('');
  if(fstr === sstr){
    return true;
  }
  return false;
}
// second
function compare(str1, str2) {
  const first_set = str1.split('');
  const second_set = str2.split('');
  let result = true;

  first_set.sort();
  second_set.sort();

  for (let i = 0; i < Math.max(first_set.length, second_set.length); i++) {
    if (first_set[i] !== second_set[i]) {
      result = false;
    }
  }

  return result;
}
// console.log(compare("xyz", "zyx"))
// console.log(compare("xyz", "zyp"))

// 100. Write a JavaScript program to check whether there is at least one element which occurs in two given sorted arrays of integers.

function checkCommon(arra1, arra2) {
  for (let i = 0; i < arra1.length; i++)
  {
    if (arra1.includes(arra2[i])) 
      return true;
  }
  return false;
}
console.log(checkCommon([1,2,3], [3,4,5]));   
console.log(checkCommon([1,2,3], [5,6,7]));
