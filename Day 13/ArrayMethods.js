// Methods
// const array = [5, 12, 8, 130, 44];

// Array.at()
// console.log(array.at(2)); //8
//================================================================

// Array.concat()
// const array2 = ['d', 'e', 'f'];
// console.log(array.concat(array2)); //[5, 12, 8, 130, 44, "d", "e", "f"]

//=================================================================

// Array.copyWithin()
// console.log(array.copyWithin(1,4)); //[ 5, 44, 8, 130, 44 ]

//=================================================================

// Array.entries()

// for (let e of array.entries()) {
//   console.log(e);
// }
//[ 0, 5 ]
// [ 1, 12 ]
// [ 2, 8 ]
// [ 3, 130 ]
// [ 4, 44 ]

//=================================================================

// Array.every()
// const ans = (clv) => clv < 140
// console.log(array.every((clv) => clv < 140)); //true
// console.log(array.every((clv) => clv > 140)); //false

//=================================================================

// Array.fill()
// console.log(array.fill(12, 3)); //[ 5, 12, 8, 12, 12 ]

//=================================================================

// Array.filter()
// console.log(array.filter(array => array > 4)); //[ 5, 12, 8, 130, 44 ]
// console.log(array.filter(array => array > 20)); //[ 130, 44 ]

//=================================================================

// Array.find()
// console.log(array.find(e => e > 20)); //130
// console.log(array.find(e => e > 4)); //5
// console.log(array.find(e => e > 130)); //undefined


//=================================================================

// Array.findIndex()
// console.log(array.findIndex(e => e > 20)); //3
// console.log(array.findIndex(e => e > 4)); //0
// console.log(array.findIndex(e => e > 130)); // -1

//=================================================================

// Array.flat()
// const arr1 = [0, 1, 2, [3, 4]];

// console.log(arr1.flat());
// // expected output: [0, 1, 2, 3, 4]

// const arr2 = [0, 1, 2, [[[3, 4]]]];

// console.log(arr2.flat(2));
// // expected output: [0, 1, 2, [3, 4]]

//=================================================================

// Array.flatMap()

//=================================================================

// Array.forEach()
// const array1 = ['a', 'b', 'c'];

// array1.forEach(element => console.log(element));

// // expected output: "a"
// // expected output: "b"
// // expected output: "c"

//=================================================================

// Array.from()
// console.log(Array.from('foo'));
// // expected output: Array ["f", "o", "o"]

// console.log(Array.from(array, x => x + x));
// // expected output: Array [ 10, 24, 16, 260, 88 ]

//=================================================================

// Array.includes()
// console.log(array.includes(44)); // true
// console.log(array.includes(123)); // false

//=================================================================

// Array.indexOf()
// const beasts = ['ant', 'bison', 'camel', 'duck', 'bison'];

// console.log(beasts.indexOf('bison'));
// // expected output: 1

// // start from index 2
// console.log(beasts.indexOf('bison', 2));
// // expected output: 4

// console.log(beasts.indexOf('giraffe'));
// // expected output: -1

//=================================================================

// Array.isArray()
// console.log(Array.isArray([1, 2, 3]));  // true
// console.log(Array.isArray('foobar'));  // false
// console.log(Array.isArray(undefined));  // false
// console.log(Array.isArray({foo: 123}));  // false

//=================================================================

// Array.join()
// console.log(array.join('')); //512813044
// const elements = ['Fire', 'Air', 'Water'];

// console.log(elements.join());
// // expected output: "Fire,Air,Water"

// console.log(elements.join(''));
// // expected output: "FireAirWater"

// console.log(elements.join('-'));
// // expected output: "Fire-Air-Water"

//=================================================================

// Array.keys()
// for(const key of array.keys()){
//     console.log(key);
// }
// 0
// 1
// 2
// 3
// 4
//=================================================================

// Array.lastIndexOf()
// console.log(array.lastIndexOf(5)); //0
// console.log(array.lastIndexOf(44)); //4

// const animals = ['Dodo', 'Tiger', 'Penguin', 'Dodo'];

// console.log(animals.lastIndexOf('Dodo'));
// // expected output: 3

// console.log(animals.lastIndexOf('Tiger'));
// // expected output: 1


//=================================================================

// Array.map()
// The map() method creates a new array populated with the results of calling a provided function on every element in the calling array.

// console.log(array.map(x=>x*2)); //[ 10, 24, 16, 260, 88 ]

//=================================================================

// Array.of()

//=================================================================

// Array.pop()
// const plants = ['broccoli', 'cauliflower', 'cabbage', 'kale', 'tomato'];

// console.log(plants.pop());
// // expected output: "tomato"

// console.log(plants);
// // expected output: Array ["broccoli", "cauliflower", "cabbage", "kale"]

// plants.pop();

// console.log(plants);
// // expected output: Array ["broccoli", "cauliflower", "cabbage"]

// console.log(array.pop()); //44
// console.log(array.pop());//130
// console.log(array.pop());//8
// console.log(array.pop());//12
// console.log(array); //[5]


//=================================================================

// Array.push()
// console.log(array.push(55));//6
// console.log(array); //[ 5, 12, 8, 130, 44, 55 ]
// console.log(array.push(55,75,59,40,36));//11
// console.log(array); //[
// //     5, 12,  8, 130, 44,
// //    55, 55, 75,  59, 40,
// //    36
// //  ]

//=================================================================

// Array.reduce()
// console.log(array.reduce((p,c) => p+c));//199


// const array1 = [1, 2, 3, 4];
// const reducer = (previousValue, currentValue) => previousValue + currentValue;

// // 1 + 2 + 3 + 4
// console.log(array1.reduce(reducer));
// // expected output: 10

// // 5 + 1 + 2 + 3 + 4
// console.log(array1.reduce(reducer, 5));
// // expected output: 15

//=================================================================

// Array.reduceRight()
// const array1 = [[0, 1], [2, 3], [4, 5]].reduceRight(
//     (accumulator, currentValue) => accumulator.concat(currentValue)
//   );
  
//   console.log(array1);
//   // expected output: Array [4, 5, 2, 3, 0, 1]

//=================================================================

// Array.reverse()
// console.log(array.reverse()); //[ 44, 130, 8, 12, 5 ]

//=================================================================

// Array.shift()
// console.log(array.shift());//5
// console.log(array);//[ 12, 8, 130, 44 ]

//=================================================================

// Array.slice()
//[ 5, 12, 8, 130, 44 ]
// console.log(array.slice(2)); //[ 8, 130, 44 ]
// console.log(array.slice(1,3));// [12,8]
// console.log(array.slice(2, -1)); //[ 8, 130 ]
// console.log(array.slice()); //[ 5, 12, 8, 130, 44 ]
// console.log(array.slice(-2)); //[ 130, 44 ]
// console.log(array.slice(6)); // []


//=================================================================

// Array.some()
// console.log(array.some(ele => ele % 2 ===0)); //true

//=================================================================

// Array.sort()
// console.log(array.sort()); //[ 12, 130, 44, 5, 8 ]
// const months = ['March', 'Jan', 'Feb', 'Dec'];
// console.log(months.sort());
// // expected output: Array ["Dec", "Feb", "Jan", "March"]

//=================================================================

// Array.splice()
// const months = ['Jan', 'March', 'April', 'June'];
// months.splice(1, 0, 'Feb');
// // inserts at index 1
// console.log(months);
// // expected output: Array ["Jan", "Feb", "March", "April", "June"]

// months.splice(4, 1, 'May');
// // replaces 1 element at index 4
// console.log(months);
// // expected output: Array ["Jan", "Feb", "March", "April", "May"]

//=================================================================

// Array.toLocaleString()
// console.log(array.toLocaleString()); //5,12,8,130,44

//=================================================================

// Array.toSource()

//=================================================================

// Array.toString()
// console.log(array.toString()); //5,12,8,130,44
// const array1 = [1, 2, 'a', '1a'];

// console.log(array1.toString());

//=================================================================

// Array.unshift()
// The unshift() method adds one or more elements to the beginning of an array and returns the new length of the array.
// console.log(array.unshift(1,5)); //7
// console.log(array);// [1, 5, 5, 12, 8, 130, 44]

//=================================================================

// Array.values()
// for (const value of array.values()) {
//     console.log(value);
//   }
//   5
// 12
// 8
// 130
// 44
//=================================================================

// Inheritance:

// Function
// Properties
// Function.arguments
// Function.caller
// Function.displayName
// Function.length
// Function.name
// Methods
// Function.apply()
// Function.bind()
// Function.call()
// Function.toSource()
// Function.toString()
// Object
// Properties
// Object.prototype.constructor
// Object.prototype.__proto__
// Methods
// Object.__defineGetter__()
// Object.__defineSetter__()
// Object.__lookupGetter__()
// Object.__lookupSetter__()
// Object.hasOwnProperty()
// Object.isPrototypeOf()
// Object.propertyIsEnumerable()
// Object.setPrototypeOf()
// Object.toLocaleString()
// Object.toSource()
// Object.toString()
// Object.valueOf()

