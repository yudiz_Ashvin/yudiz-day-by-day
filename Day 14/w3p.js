// 102. Write a JavaScript program to find the number of inversions of a given array of integers.

// Note: Two elements of the array a stored at positions i and j form an inversion if a[i] > a[j] and i < j.
//    0  1  2  3  4   i < j
// 1)[0, 3, 2, 5, 9]  a[i] > a[j]

// (0>3) = false = 0 same as (3>2) = true =1
// (0>2) = false = 0         (3>5) = false = 0.... fro all  
// (0>5) = false = 0
// (0>9) = false = 0

function inversion(arr) {
    var count = 0;
    for (var i = 0; i < arr.length; i++) {
        for (var j = i + 1; j < arr.length; j++) {
            if (arr[i] > arr[j])
                count++;
        }
    }
    return count;
}
// console.log(inversion([0, 3, 2, 5, 9]));
// console.log(inversion([1, 5, 4, 3]));
// console.log(inversion([10, 30, 20, -10]));

// 103. Write a JavaScript program to find the maximal number from a given positive integer by deleting exactly one digit of the given number.

function digit_del(num) {
    let str_num = String(num);
    res = 0;
    for (i = 0; i < str_num.length; i++) {
        res = Math.max(res, Number(str_num.substring(0, i) + str_num.substring(i + 1, str_num.length)));
    }
    return res;
}
// console.log(digit_del(100)); // 10
// console.log(digit_del(10)); // 1
// console.log(digit_del(1245)); // 245
// console.log(digit_del(120)); // 20
// console.log(digit_del(9245)); // 945