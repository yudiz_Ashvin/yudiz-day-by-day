// let a = 1 ;
// setTimeout(() => {
//     console.log(a);
//     a=2;
// },0)

// const p  = new Promise(resolve => {
//     console.log(a);
//     a=3;
//     resolve()
// })

// p.then(() => console.log(a))
// console.log(a);

//===============================

// if a network request returns and it has an attached callback, where dose that callback gets pushed into?

// callback queue

//===============================

// Promise.all is rejected if any of the elements are rejected.Promised

// yes

//===============================
// today's _______ computer, smartphones and tablets enable computer to perform tasiks truly concurrently.
// //multicore

//===============================

// const container = document.getElementById('container'),
// button = document.getElementById('button')

// button.addEventListener('click', () => {
//     Promise.resolve().then(() => console.log('A'))
//     console.log("B");
// })

// container.addEventListener('click', () => console.log("c"))

// b a c

//===============================

// tasks that proceed independently of one another are said to execute ___________ and are referred to as _______ tasks.

// asynchronously, asynchronous

//===============================

// const promise = new Promise((res, rej) => {
//     rej(Error('some error occurred'));
// })
// promise.catch(error => console.log(error.message));
// // promise.then(error => console.log(error.message)); //some error occurred
// promise.catch(error => console.log(error.message));

// //some error occurred, some error occurred

//===============================

// var p = new Promise(function(){
//     return "ok";
// });
// var p2 = p.then(function(data){
//     return data;
// });

// var p3 =p2.then(function(data){
//     return data + " Bye";
// });
// // OK Bye

//===============================

// what is the function to stop a setTimeout timer?
// clearTimeout()

//===============================

// const p = new Promise((res,rej) => {
//     rej(Error('Some Error Occurred'))
// }).catch(error => console.log(error))
// .then(error => console.log(error));
// // Some Error Occurred, undefined

//===============================

// var p = new Promise(function(res, rej) {
//     setTimeout(function() {
//         res("OK");
//     },2000);
// }).then();

// var p2 = p.then(function(data) {
//     return data + " Good";
// })
// //no log

//===============================

// var p1 =new Promise(function(res,rej) {
//     setTimeout(res, 500, 'one');
// });

// var p2 =new Promise(function(res,rej) {
//     setTimeout(res, 100, 'two');
// });
// Promise.race([p1, p2]).then(function(value){
//     console.log(value);
// })

//two

//===============================

// //A
// const add = (x,y, callback) => {
//     setTimeout(() => {
//         callback(x +y);
//     }, 2);
// }
// //B
// const add = (x,y, callback) => {
//     setTimeout(() => {
//         callback(x +y);
//     }, 2000);
// }
// //c
// const add = (x,y, callback) => {
//     setTimeout(() => {
//         callback(x +y,2);
//     });
// }
// //D
// const add = (x,y, callback) => {
//     setTimeout(() => {
//         callback(x +y,2000);
//     });
// }

// b

//===============================

// converting a simple function to a promise is termed as?
// promisify

//===============================

// async function f(){
//     let result = 'first';
//     let promise = new Promise((res,rej) =>{
//         setTimeout(() => res('done'), 1000);
//     });
//     result = await promise;

// console.log(result);
// }
// f();

//done

//===============================

// Select the code which executes the callback inside makeMeal function


// function makeMeal(meal, cb){
//     alert(`Preparing ${meal}.`);
    // inside cb code hear
    // setTimeout(()=>{
    //         makeMeal()
    // }, 2000)
// }
// makeMeal('lunch', () => {
//     alert('finished the meal!')
// });

// cb()


//===============================

// var p = new Promise(function (res,rej){
//     setTimeout(function(){
//         res('ok');
//     },2000);
// });

// p.then().then(function(data) {
//     console.log(data);
// })
// //ok

//===============================

// how dose the event loop decide what function to run?
// Queue

//===============================

// function job(){
//     return new Promise(function(resolve,reject) {
//         reject();
//     });
// }
// let promise = job();
// promise.then(function(){
//     console.log('Success 1');
// })
// .then(function(){
//     console.log('Success 2');
// })
// .then(function(){
//     console.log('Success 3');
// })
// .catch(function(){
//     console.log('error 1');
// })
// .then(function(){
//     console.log('Success 4');
// })

//error1,success 4

//===============================

// setTimeout(() => console.log('timeout'),0)

// Promise.resolve().then(()=> console.log('promise'))

// //promise, timeout

//===============================

// in this code:
// x=3;
// y=4;
// is it possible for the event loop to interrupt x=3 and run somthing else?

// NO

//===============================

// if you just have 1 event loop in your program and no other ConvolverNode, is it possible for 2 line of your code to run at the exact same time?

// NO
//===============================

// what is a Promise?
// object
//===============================


// asynchronous

//===============================

// var p = new Promise(function(res,rej) {
//     res("ok")
// });
// var p2 = p.then(function(data) {
//     return data;
// });
// var p3 = p.then(function(res,rej) {
//     return data;
// });
// console.log(p2 === p3);

// //false
