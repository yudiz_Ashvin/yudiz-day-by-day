//🤔Generator(yield)

//👉 Generator is a pausable⏸  function
// freezes🥶 when yield keyword shows🐈 up

/************************Code block 1***********************/

// function* gen() {
// 	console.log('🖐 there');
// 	yield;
// 	console.log('Hello 😀');
// }

// let iterator = gen();
// iterator.next(); // 🖐 there
// iterator.next();// Hello 😀

/* 
💡💡In ES6, for iterating through the loops, a new for of loop has been added, do check that out.
 */

/***************************Code block 2********************/

// function* iCanFly() {
// 	console.log('Look at me, I am flying 🛸');
//     yield;
// 	console.log('Wait what!🎮 You are making me fly!🪁');
//     yield;
// 	console.log('Okay stop it!🥴');
//     //return undefined;
// }

// let iterator = iCanFly()
// iterator.next() // {value : 1, done : false}
// console.log("text1")
// iterator.next() // {value : 2, done : false}
// console.log("text2")
// const result  = iterator.next() // {value : 3, done : false}
// console.log("text3", result)
// iterator.next() // {value : undefined, done : true}
// console.log("text4")


// iterator.next(); //Look at me, I am flying 🛸
// iterator.next(); //Wait what!🎮 You are making me fly!🪁
// iterator.next(); //Okay stop it!🥴



/***************************Code block 3********************/

// function* generator() {
// 	let value1 = 1 + (yield);
// 	console.log('value1', value1);
// 	let value2 = 1 + (yield);
// 	console.log('value2', value2);
//     yield value1+value2;
// 	// return {value: value1 + value2, done :false };
// }

// const it = generator();
// console.log(it.next());//{ value: undefined, done: false }
// console.log('value after first call', it.next(40).value); //value1 41 //value after first call undefined
// console.log('value after second call', it.next(20).value); //value2 21 //value after second call 62
// console.log(it.next().done); //true

/*

What if I pass a value on line 46, what will it do ? 

Every pause is asking a ❓question and every resume is giving it an answer answer

💡💡Its not always necessary to complete the generator function, its okay to return midway and let the function get garbage collected.

✅ Generators are producers, and we can consume it on the outside by calling it.next()
 */

/***************************Code block 4********************/

// function* generator() {
// 	let value1 = 1 + (yield getData(10));
// 	console.log('value1', value1);
// 	let value2 = 1 + (yield getData(30));
// 	console.log('value2', value2);
// 	yield getData(value1 + value2);
// }

// let it = generator();
// it.next();

// function getData(d) {
// 	setTimeout(function() {
// 		console.log('Before returning the value');
// 		console.log('After value has been sent', it.next().value);
// 	}, 2000);
// }

/* 🕺 Synchronous, Sequential, Blocking looking code  */

/***************************📣 okay, okay. Last block 🏃‍♀️ ********************/



function* generatorWithPromise() {
	let value1 = 1 + (yield genieGetMeSomething());
	let value2 = 1 + (yield genieGetMeSomething());
	yield genieGetMeSomething(value1 + value2);
}

let it = generatorWithPromise();
it.next()

function genieGetMeSomething() {
	const myPromise = it.next().value
    myPromise.then((result)=>{
        it.next(result)
    });
}











/*
 🙄😥Making cancellable promises - how to do that! 
*/
