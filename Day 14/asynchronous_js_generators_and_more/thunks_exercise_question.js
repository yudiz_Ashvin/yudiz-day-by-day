function fakeApiCall(url, cb) {
	let fake_responses = {
		file1: 'The first text',
		file2: 'The middle text',
		file3: 'The last text'
	};
	let randomDelay = Math.round(Math.random() * 1e4) % 8000 + 1000;

	console.log('Requesting: ' + url);

	function getData() {
		cb(fake_responses[url]);
	}
	setTimeout(getData, randomDelay);
}

function output(text) {
	console.log(text);
}

// **************************************

function getFile(file) {
	let resp=undefined

	fakeApiCall(file, function(text) {
		if (!resp) resp = text;
		else resp(text);
	});

	return function th(cb) {
		if (resp) cb(resp);
		else resp = cb;
	};
}
// **************************************


let th1 = getFile('file1');
let th2 = getFile('file2');
let th3 = getFile('file3');

th1(function ready(t1) {
	output(t1);
	th2(function ready(t2) {
		output(t2);
		th3(function ready(t3) {
			output(t3);
			output('Complete!');
		});
	});
});

/*********

1
2
3

*/




