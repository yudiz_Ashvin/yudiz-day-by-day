// `➡ Run to Completion Semantic - only a single function runs at a time
// - one of the most important characteristics of javascript`

// one of the most important things that is built into javascript

// `🛑🛑Generators do not have run to completion semantic`
// `Syntactic form of a state machine` - State machines - patterned series of flow from 1 state to another to another

//👉 Generator is a pausable⏸  function
// freezes🥶 when yield keyword shows🐈 up

//👷‍♂️🛑Blocking is extremely localized.

// About generators
// - What we have essentially done is taken synchronicity itself and factored it out as an implementation detail, that we don't care about.

// - Promises solve out inversion of control issue, trust issue, callback hell issue
// - generators solve out non-local, non sequential issue
