// Synchronous thunk

// function add(x,y){
//     return x+y
// }

// let thunk = function(){
//     return add(10,15)
// }

// thunk()




/*********************************************************/

//Asynchronous thunk

// function addAsync(x,y,callback){
//     function doSomething(){
//         callback(x + y)
//     }
//     setTimeout(doSomething, 1000)
// }

// let thunk = function(cb){
//     addAsync(10,15,cb)
// }

// function callbackFunction(result){
//     console.log(`Result is ${result}😄`)
// }


// thunk(callbackFunction)

/*********************************************************/

function makeThunk(fn){
    console.log("arguments", arguments) //array of arguments
    let args = [].slice.call(arguments, 1)   

    console.log("args", args)
    
    function resultResolver(callback){
        args.push(callback) 
        console.log("args inside thunkFunc", args)
        fn.apply(null,args)
        //addAsync(10,15,callback)
    }
    return resultResolver
}

function addAsync(value1, value2, callback){
    function addValues(){
        callback(value1 + value2)
    }
    setTimeout(addValues, 1000)
}

// const thunk = makeThunk(addAsync, 10, 15)


resultResolver = makeThunk(addAsync,)
thunk(function(sum){
    console.log(`Sum is ${sum}`)
})

/*********************************************************/

// "🔎🔎 : https://stackoverflow.com/a/1986"



