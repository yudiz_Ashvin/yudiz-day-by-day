// - CSP - Managing Concurrency - Communicating📶 Sequential🔢 Processes♻
// - back🔙pressure🤢 - reactive programming

let ch = channel();

function* process1() {
	yield put(ch, 'Hello');
	let msg = yield take(ch);
	console.log(msg);
}

function* process2() {
	//doing some other task
	let greeting = yield take(ch);
	yield put(ch, greeting + 'there');
	console.log('done');
}

const it1 = process1();
const it2 = process2();

while (true) {
	it1().next();
}

while (true) {
	it2().next();
}
